# `*` Recherche rapide

`*` en mode commande: permet de faire une recherche sur le mot qui se trouve 
devant ou sous le curseur.   
Possible de se parcourir les différentes occurences en utilisant n 
(occurence suivante) ou N (occurence précédente).

# `q:` Historique de commande

`q:` en mode commande : permet d'ouvrir l'aperçu de l'historique de commande.
Une fois ouvert, il peut être quitté par `:q`.
Il est possible d'éditer une commande ainsi que de les rejouer en appuyant sur 
la touche entrée.

# `<x>s/<1>/<2>/<options>` Rechercher & remplacer

`<x>s/<1>/<2>/<options>` en mode commande permet de chercher 
la première occurence de l'expression régulière `<1>` et de la remplacer par l'expression `<2>`.

`<x>` permet de déterminer la plage de recherche :

* ` ` recherche sur la ligne en cours
* `%` recherche dans tout le fichier
* `12,15` recherche de la ligne 12 à la ligne 15
* `.,$` recherche de la ligne courante à la fin du fichier
* `.,+2` recherche sur trois lignes à partir de la ligne courante 

Les options possibles (cumulables) :

* `g` traite chaque occurence de la ligne 
* `c` demande de confirmation avant chaque remplacement

Termes de recherche et remplacement :

* `\(<pattern>\)` dans `<1>` permet de retenir la valeur du pattern. Cette valeur
pourra être utilisée dans le `<2>` par la commande `\1` pour le premier pattern,
`\2` pour le second, etc...
`:%s/l_\([1-9]\)_tmp/j_\1_tmp/` permet donc de remplacer dans tout le fichier les 
occurences de l_\<chiffre>_tmp par j_\<chiffre>_tmp en conservant la valeur du 
chiffre.


# `:b <x>` Navigation dans les buffers

`:b <x>` permet de naviguer entre les différents buffers ouverts.
Par exemple, en ouvrant un fichier avec `:edit <name>`, un nouveau buffer est ouvert.

`<x>` peut être soit un nom de fichier, soit un numéro de buffer.
`:b 2` permet donc d'afficher le second buffer.

`:bnext` et `:bn` permettent d'accéder au prochain buffer

`:blast` et `:bl` permettent d'accéder au précédent buffer

`:badd <name>` permet d'ajouter parmi les buffers le fichier `<name>`

`:bdelete <x>` permet de supprimer un buffer, où `<x>` est un nom de fichier ou un 
numéro de buffer.

`:ls`, `:buffers ` et `:files` permettent d'afficher la liste des buffers
